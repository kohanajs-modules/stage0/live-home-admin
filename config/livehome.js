const {KohanaJS} = require("kohanajs");

module.exports = {
  ping_file: `${KohanaJS.EXE_PATH}/../public/media/ping/update.json`,
  homepages : new Set(['register', 'countdown', 'live', 'thank_you']),
  default_home : 'register',
  url_param_no_redirect :  "no-redirect"
};
