# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.0.0 (2022-03-09)


### ⚠ BREAKING CHANGES

* add routes.js

* add routes.js ([47b5aa1](https://gitlab.com/kohana-js/proposals/level0/live-home-admin/commit/47b5aa17a7facfc2512a40e91e428ef7749a89ed))

## [1.0.0] - 2021-10-12
### Added
- create CHANGELOG.md