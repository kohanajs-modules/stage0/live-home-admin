const { RouteList } = require('@kohanajs/mod-route');

RouteList.add('/admin/homepage', 'controller/admin/LiveHome');
RouteList.add('/admin/homepage/set/:id', 'controller/admin/LiveHome', 'change');