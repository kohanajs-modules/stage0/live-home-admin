const {ControllerMixinView, KohanaJS} = require('kohanajs');

const ControllerAdmin = KohanaJS.require('ControllerAdmin');
const ControllerMixinAdminTemplates = KohanaJS.require('controller-mixin/AdminTemplates');

const {readFile, writeFile} = require('fs/promises');
const path = require('path');

class ControllerAdminLiveHome extends ControllerAdmin{
  constructor(request){
    super(request);

    this.state.get(ControllerMixinAdminTemplates.TEMPLATES).set('index', 'templates/admin/homepage');
  }

  async before(){
    const ping = await readFile(path.normalize(KohanaJS.config.livehome.ping_file), 'utf-8');
    this.ping = JSON.parse(ping || "{}");
  }

  async action_index(){
    //read from ping/update.json
    let payload = this.ping.payload;

    let id = payload.home;
    if(payload.state) id = payload.home +":" + payload.state;

    Object.assign(
      this.state.get(ControllerMixinView.TEMPLATE).data,
      {id}
    )
  }

  async action_change(){
    //read from ping/update.json
    const ping = this.ping;

    if(!ping.payload)ping.payload = {};
    if(!ping.meta)ping.meta = {}

    const params = this.request.params.id.split(':');
    const home = params[0];
    const state = params[1] ?? "";
    ping.payload.home = home;
    ping.payload.state= state;
    ping.meta.last_update = Date.now();

    await writeFile(path.normalize(KohanaJS.config.livehome.ping_file), JSON.stringify(ping), 'utf-8');

    await this.redirect('/admin/homepage');
  }
}

module.exports = ControllerAdminLiveHome;