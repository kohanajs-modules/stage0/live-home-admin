const { KohanaJS } = require('kohanajs');
KohanaJS.initConfig(new Map([
  ['livehome', require('./config/livehome')],
]));